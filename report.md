| #    | Stage                 | Start date | End date   | Comment |
| ---- | --------------------- |------------|------------| ------- |
| 1    | Task Clarification    | 15.05.2023 | 17.05.2023 |         |
| 2    | Analysis              | 17.05.2023 | 20.05.2023 |         |
| 3    | Use Cases             | 20.05.2023 | 24.05.2023 |         |
| 4    | Search for Solutions  | 24.06.2023 | 31.05.2023 |         |
| 5    | Software Development  | 01.06.2023 | 08.06.2023 |         |
| 6    | Development Completion| 08.06.2023 | 09.06.2023 |         |
| 7    | Presentation          | 09.06.2023 | 09.06.2023 |         |
****