package edu.itpu.fopjava_course_work.rowmapper;

import edu.itpu.fopjava_course_work.dto.WarehouseDataDto;

/**
 * @author mitprog1
 *
 *         Transforms the single row fetched from the csv file
 *         into {@link WarehouseDataDto} object
 *
 */
public class CustomStringToWarehouseDataRowMapper {

    public WarehouseDataDto mapRaw(String[] row) {
        WarehouseDataDto sd = new WarehouseDataDto();

        sd.setId(row[0]);
        sd.setName(row[1]);
        sd.setSize(row[2]);
        sd.setColor(row[3]);
        sd.setMaterial(row[4]);
        sd.setPrice(row[5]);

        return sd;
    };
}
