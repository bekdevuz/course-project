package edu.itpu.fopjava_course_work;

import edu.itpu.fopjava_course_work.dao.WarehouseDao;
import edu.itpu.fopjava_course_work.dao.impl.WarehouseCsvBasedDaoImpl;
import edu.itpu.fopjava_course_work.dto.WarehouseDataDto;
import edu.itpu.fopjava_course_work.service.WarehouseService;
import edu.itpu.fopjava_course_work.service.impl.WarehouseServiceImpl;
import edu.itpu.fopjava_course_work.exception.WarehouseException;


import java.util.List;
import java.util.Scanner;
import java.util.Comparator;

public class EntryPoint {

    private static WarehouseService warehouseService;

    public static void main(String[] args) {
        // Initialize the warehouse service
        WarehouseDao warehouseDao = new WarehouseCsvBasedDaoImpl();
        warehouseService = new WarehouseServiceImpl(warehouseDao);

        // Display application information and available commands
        displayApplicationInfo();
        displayAvialableCommands();

        // Start the command input loop
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        while (!exit) {
            System.out.print("Enter a command: ");
            String input = scanner.nextLine();
            exit = executeCommand(input);
        }

        // Close the scanner
        scanner.close();
    }

    // Display application information
    private static void displayApplicationInfo() {
        System.out.println("Bed Linens and Dishes Warehouse / version 1.0.0 / 01-06-2023");
        System.out.println("Developer: Asadbek Savronov");
        System.out.println("Email: asadbek_savronov@student.itpu.uz");
        System.out.println();
    }

    // Display available commands
    private static void displayAvialableCommands() {
        System.out.println("Available commands:");
        System.out.println("search <parameter> - searches by the given parameter (Ex: search red or search 123)");
        System.out.println("list <order> - lists all products ordered by the given order (Ex: asc or desc)");
        System.out.println("clear - clears the cli");
        System.out.println("exit - exits the application");
        System.out.println();
    }

    // Execute available commands
    private static boolean executeCommand(String input) {
        String[] parts = input.split(" ", 2);
        String command = parts[0].trim().toLowerCase();

        switch (command) {
            case "search":
                if (parts.length < 2) {
                    System.out.println("Invalid command. Please provide a parameter for searching.");
                    break;
                }
                String parameter = parts[1].trim();
                searchProducts(parameter);
                break;

            case "list":
                if (parts.length < 2) {
                    System.out.println("Invalid command. Please provide an order for listing. (asc or desc)");
                    break;
                }
                String order = parts[1].trim();
                listProducts(order);
                break;

            case "clear":
                clearScreen();
                displayApplicationInfo();
                displayAvialableCommands();
                break;

            case "exit":
                System.out.println("Exiting the application...");
                return true;

            default:
                System.out.println("Invalid command. See the available commands below!");
                displayAvialableCommands();
        }

        return false;
    }


    // Search the products by the given parameter
    private static void searchProducts(String parameter) {
        List<WarehouseDataDto> products = warehouseService.retrieveWarehouseDataFilteredByParameter(parameter);

        if (products.isEmpty()) {
            System.out.println("No products found for the given parameter.");
        } else {
            System.out.println("Search Results for " + parameter + ":" + "("+ products.size() + ")");
            for (WarehouseDataDto product : products) {
                System.out.println(product);
            }
        }

        System.out.println();
    }

    // List all the products ordered by the given order
    private static void listProducts(String order) {
        List<WarehouseDataDto> products = warehouseService.retrieveAllWarehouseData();

        if (products.isEmpty()) {
            System.out.println("No products found.");
        } else {
            // Sort the products based on the specified order
            if (order.equalsIgnoreCase("asc")) {
                products.sort(Comparator.comparing(WarehouseDataDto::getName));
            } else if (order.equalsIgnoreCase("desc")) {
                products.sort(Comparator.comparing(WarehouseDataDto::getName).reversed());
            } else {
                System.out.println("Invalid order. Please enter 'asc' or 'desc'.");
                return;
            }

            System.out.println("All Products:");
            for (WarehouseDataDto product : products) {
                System.out.println(product);
            }
        }

        System.out.println();
    }

    // Clear the screen
    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
