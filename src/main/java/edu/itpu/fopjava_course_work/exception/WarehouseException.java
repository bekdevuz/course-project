package edu.itpu.fopjava_course_work.exception;

public class WarehouseException extends Exception {
    public WarehouseException(String message) {
        super(message);
    }
}
