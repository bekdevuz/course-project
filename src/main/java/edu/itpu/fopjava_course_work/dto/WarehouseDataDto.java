package edu.itpu.fopjava_course_work.dto;

import java.util.Objects;

public class WarehouseDataDto {
    // id,name,size,color,material,price

    private String id;
    private String name;
    private String size;
    private String color;
    private String material;
    private String price;

    public WarehouseDataDto() {
    }

    public WarehouseDataDto(String id, String name, String size, String color, String material, String price) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.color = color;
        this.material = material;
        this.price = price;
    }

    @Override
    public String toString() {
        return 
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", size='" + size + '\'' +
                ", color='" + color + '\'' +
                ", material='" + material + '\'' +
                ", price='" + price + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof WarehouseDataDto))
            return false;
        WarehouseDataDto that = (WarehouseDataDto) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName())
                && Objects.equals(getSize(), that.getSize()) && Objects.equals(getColor(), that.getColor())
                && Objects.equals(getMaterial(), that.getMaterial()) && Objects.equals(getPrice(), that.getPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getSize(), getColor(), getMaterial(), getPrice());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
