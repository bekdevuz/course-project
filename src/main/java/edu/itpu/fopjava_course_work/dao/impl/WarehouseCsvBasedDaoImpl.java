package edu.itpu.fopjava_course_work.dao.impl;

import edu.itpu.fopjava_course_work.dao.WarehouseDao;
import edu.itpu.fopjava_course_work.dto.WarehouseDataDto;
import edu.itpu.fopjava_course_work.rowmapper.CustomStringToWarehouseDataRowMapper;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WarehouseCsvBasedDaoImpl implements WarehouseDao {


//    path from repo root can be used for the case, you're running the app via entry point from the IDE
     private static final String PATH_TO_DATA_SOURCE = "src/main/resources/csv/BedLinens.csv";

    //absolute path should be used for the case, you're running compiled jar file.
    // private static final String PATH_TO_DATA_SOURCE = "/Users/bek/coding/univer/final/Course Project/src/main/resources/csv/BedLinens.csv";


    @Override
    public List<WarehouseDataDto> retrieveAllWarehouseDataFromDataSource() {

        List<String[]> allData = fetchDataFromCsvFile();

        removeHeaderDataFromPureDataSet(allData);

        List<WarehouseDataDto> sDataResulted = transformStringDataIntoDtoBasedCollection(allData);
        return sDataResulted;
    }

    private static List<String[]> fetchDataFromCsvFile() {
        FileReader filereader = null;
        try {
            filereader = new FileReader(PATH_TO_DATA_SOURCE);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        CSVParser parser = new CSVParserBuilder()/*.withSeparator(rawSeparator)*/.build();
        CSVReader csvReader = new CSVReaderBuilder(filereader)
                .withCSVParser(parser)
                .build();

        List<String[]> allData;
        try {
            allData = csvReader.readAll();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (CsvException e) {
            throw new RuntimeException(e);
        }
        return allData;
    }

    private List<WarehouseDataDto> transformStringDataIntoDtoBasedCollection(List<String[]> allData) {
        CustomStringToWarehouseDataRowMapper rowMapper =new CustomStringToWarehouseDataRowMapper();
        List<WarehouseDataDto> result = new ArrayList<>();

//        for (String[] row : allData.subList(0,5)) {
        for (String[] row : allData) {
            WarehouseDataDto sd = rowMapper.mapRaw(row);

            result.add(sd);
        }
         return result;
    }

    private void removeHeaderDataFromPureDataSet(List<String[]> allData) {
        allData.remove(0);
    }
}
