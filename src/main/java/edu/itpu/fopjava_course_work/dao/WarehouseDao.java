package edu.itpu.fopjava_course_work.dao;

import edu.itpu.fopjava_course_work.dto.WarehouseDataDto;

import java.util.List;

/**
 * @author mitprog1
 *
 * Fetching all data from data source
 *
 * */
public interface WarehouseDao {

    List<WarehouseDataDto> retrieveAllWarehouseDataFromDataSource();
}
