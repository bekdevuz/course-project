package edu.itpu.fopjava_course_work.service.impl;

import edu.itpu.fopjava_course_work.dao.WarehouseDao;
import edu.itpu.fopjava_course_work.dto.WarehouseDataDto;
import edu.itpu.fopjava_course_work.exception.WarehouseException;
import edu.itpu.fopjava_course_work.service.WarehouseService;

import java.util.ArrayList;
import java.util.List;

public class WarehouseServiceImpl implements WarehouseService {

    private WarehouseDao dao;

    public WarehouseServiceImpl(WarehouseDao dao) {
        this.dao = dao;
    }

    @Override
    public List<WarehouseDataDto> retrieveAllWarehouseData() {
        try {
            return dao.retrieveAllWarehouseDataFromDataSource();
        } catch (Exception e) {
            // Handle the exception and return an empty list or log the error
            System.err.println("Error retrieving warehouse data (all): " + e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<WarehouseDataDto> retrieveWarehouseDataFilteredByParameter(String parameter) {
        try {
            List<WarehouseDataDto> all = dao.retrieveAllWarehouseDataFromDataSource();
            return filterByParameter(all, parameter);
        } catch (Exception e) {
            // Handle the exception and return an empty list or log the error
            System.err.println("Error retrieving warehouse data (by param): " + e.getMessage());
            return new ArrayList<>();
        }
    }

    private List<WarehouseDataDto> filterByParameter(List<WarehouseDataDto> all, String parameter) {
        List<WarehouseDataDto> filtered = new ArrayList<>();

        for (WarehouseDataDto data : all) {
            if (containsParameter(data, parameter)) {
                filtered.add(data);
            }
        }

        return filtered;
    }

    private boolean containsParameter(WarehouseDataDto data, String parameter) {
        return data.getName().equalsIgnoreCase(parameter)
                || data.getId().equalsIgnoreCase(parameter)
                || data.getColor().equalsIgnoreCase(parameter)
                || data.getSize().equalsIgnoreCase(parameter)
                || data.getMaterial().equalsIgnoreCase(parameter)
                || data.getPrice().equalsIgnoreCase(parameter);
    }
}
