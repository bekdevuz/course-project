package edu.itpu.fopjava_course_work.service;

import edu.itpu.fopjava_course_work.dto.WarehouseDataDto;

import java.util.List;

/**
 *
 * @author mitprog1
 *
 *         manipulates/aggregates, then returns the final result set;
 *
 */
public interface WarehouseService {

    /**
     * returns pure data set, which was fetched from the data source
     */
    List<WarehouseDataDto> retrieveAllWarehouseData();

    /**
     * returns the data set, filtered by exact parameter
     */
    List<WarehouseDataDto> retrieveWarehouseDataFilteredByParameter(String parameter);
}
